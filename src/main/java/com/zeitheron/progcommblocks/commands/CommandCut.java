package com.zeitheron.progcommblocks.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

public class CommandCut extends CommandBase
{
	@Override
	public String getName()
	{
		return "cutstr";
	}
	
	@Override
	public String getUsage(ICommandSender sender)
	{
		return "/cutstr [from] [string] OR /cutstr [from] [to] [string]";
	}
	
	@Override
	public int getRequiredPermissionLevel()
	{
		return 0;
	}
	
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		int ints = 1;
		int from = CommandBase.parseInt(args[0]);
		int to = -1;
		try
		{
			to = CommandBase.parseInt(args[1]);
			++ints;
		} catch(Throwable throwable)
		{
		}
		String s = "";
		for(int i = ints; i < args.length; ++i)
			s = s + args[i] + " ";
		if(s.endsWith(" "))
			s = s.substring(0, s.length() - 1);
		sender.sendMessage(new TextComponentString(to > from ? s.substring(from, to) : s.substring(from)));
	}
}
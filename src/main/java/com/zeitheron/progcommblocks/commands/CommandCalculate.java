package com.zeitheron.progcommblocks.commands;

import com.zeitheron.hammercore.utils.math.ExpressionEvaluator;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

public class CommandCalculate extends CommandBase
{
	@Override
	public String getName()
	{
		return "calculate";
	}
	
	@Override
	public String getUsage(ICommandSender sender)
	{
		return "/calculate [expression]";
	}
	
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		String expression = "";
		for(String i : args)
			expression = expression + i + " ";
		expression = expression.trim();
		sender.sendMessage(new TextComponentString(ExpressionEvaluator.evaluate(expression)));
	}
	
	@Override
	public int getRequiredPermissionLevel()
	{
		return 0;
	}
	
	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return true;
	}
}
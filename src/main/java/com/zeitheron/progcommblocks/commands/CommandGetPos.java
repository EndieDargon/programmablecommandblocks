package com.zeitheron.progcommblocks.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

public class CommandGetPos extends CommandBase
{
	@Override
	public String getName()
	{
		return "getpos";
	}
	
	@Override
	public String getUsage(ICommandSender sender)
	{
		return "/getpos [entity UUID | entity selector | player name]";
	}
	
	@Override
	public int getRequiredPermissionLevel()
	{
		return 2;
	}
	
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		if(args.length == 0)
		{
			sender.sendMessage(new TextComponentString("" + sender.getPositionVector().x + " " + sender.getPositionVector().y + " " + sender.getPositionVector().z));
		} else
		{
			Entity e = CommandBase.getEntity(server, sender, args[0], Entity.class);
			sender.sendMessage(new TextComponentString("" + e.posX + " " + e.posY + " " + e.posZ));
		}
	}
}

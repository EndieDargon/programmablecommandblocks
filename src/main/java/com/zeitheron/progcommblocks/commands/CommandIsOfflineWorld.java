package com.zeitheron.progcommblocks.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.integrated.IntegratedServer;

public class CommandIsOfflineWorld extends CommandBase
{
	@Override
	public String getName()
	{
		return "ifoffline";
	}
	
	@Override
	public String getUsage(ICommandSender sender)
	{
		return "/ifoffline";
	}
	
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		IntegratedServer is;
		if(!server.isDedicatedServer() && (is = (IntegratedServer) server).getPublic())
			throw new CommandException("Server is online");
		CommandBase.notifyCommandListener(sender, this, 0, "Server is offline");
	}
	
	@Override
	public int getRequiredPermissionLevel()
	{
		return 2;
	}
}
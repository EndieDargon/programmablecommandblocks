package com.zeitheron.progcommblocks.gui;

import java.io.IOException;

import javax.annotation.Nullable;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.progcommblocks.logic.PCBCommand;
import com.zeitheron.progcommblocks.network.PacketEditPCBCommand;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.ITabCompleter;
import net.minecraft.util.TabCompleter;
import net.minecraft.util.math.BlockPos;

public class GuiEditPCB extends GuiScreen implements ITabCompleter
{
	protected int xSize;
	protected int ySize;
	protected int guiLeft;
	protected int guiTop;
	private TabCompleter tabCompleter;
	public GuiTextField command;
	public final PCBCommand pcb;
	public final GuiPCB parent;
	
	public GuiEditPCB(PCBCommand pcb, GuiPCB parent)
	{
		this.parent = parent;
		this.pcb = pcb;
		this.xSize = 200;
		this.ySize = 60;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
		this.command = new GuiTextField(0, this.fontRenderer, this.guiLeft, this.guiTop, 200, 20);
		this.command.setMaxStringLength(65534);
		this.command.setText(this.pcb.command);
		this.addButton(new GuiButton(0, this.guiLeft, this.guiTop + 40, 40, 20, "Commit"));
		this.addButton(new GuiButton(1, this.guiLeft + 160, this.guiTop + 40, 40, 20, "Cancel"));
		this.tabCompleter = new TabCompleter(this.command, true)
		{
			
			@Override
			@Nullable
			public BlockPos getTargetBlockPos()
			{
				return GuiEditPCB.this.parent.tile.getPos();
			}
		};
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.drawDefaultBackground();
		this.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		this.command.drawTextBox();
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		this.command.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		this.tabCompleter.resetRequested();
		if(keyCode == 15)
		{
			this.tabCompleter.complete();
		} else
		{
			this.tabCompleter.resetDidComplete();
		}
		if(!this.command.textboxKeyTyped(typedChar, keyCode) && keyCode == 1)
		{
			this.mc.displayGuiScreen(this.parent);
			this.parent.mouseGrabbed = false;
		}
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 0)
		{
			PacketEditPCBCommand xy = new PacketEditPCBCommand();
			xy.world = this.parent.tile.getWorld().provider.getDimension();
			xy.pos = this.parent.tile.getPos();
			xy.command = this.command.getText();
			xy.x = this.pcb.x;
			xy.y = this.pcb.y;
			this.parent.mouseReleased(0, 0, 0);
			this.mc.displayGuiScreen(this.parent);
			this.parent.mouseReleased(0, 0, 0);
			HCNet.INSTANCE.sendToServer(xy);
		}
		if(button.id == 1)
		{
			this.mc.displayGuiScreen(this.parent);
			this.parent.mouseGrabbed = false;
		}
	}
	
	@Override
	public void setCompletions(String... newCompletions)
	{
		this.tabCompleter.setCompletions(newCompletions);
	}
}
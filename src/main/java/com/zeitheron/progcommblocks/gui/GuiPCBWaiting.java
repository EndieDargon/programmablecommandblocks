package com.zeitheron.progcommblocks.gui;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.progcommblocks.blocks.tile.TilePCB;
import com.zeitheron.progcommblocks.network.PacketRefreshGuiPCB2;

import net.minecraft.client.gui.GuiScreen;

public class GuiPCBWaiting extends GuiScreen
{
	protected int xSize;
	protected int ySize;
	protected int guiLeft;
	protected int guiTop;
	public final TilePCB pcb;
	
	public GuiPCBWaiting(TilePCB pcb)
	{
		this.pcb = pcb;
		this.xSize = 100;
		this.ySize = 18;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
		PacketRefreshGuiPCB2 xy = new PacketRefreshGuiPCB2();
		xy.pos = this.pcb.getPos();
		xy.world = this.pcb.getWorld().provider.getDimension();
		HCNet.INSTANCE.sendToServer(xy);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.drawDefaultBackground();
		this.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		String dots = "";
		int i = 0;
		while(i < System.currentTimeMillis() % 3000 / 1000)
		{
			dots = dots + ".";
			++i;
		}
		this.drawCenteredString(this.fontRenderer, "Waiting" + dots, this.guiLeft + this.xSize / 2, this.guiTop + 9, 16777215);
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
}

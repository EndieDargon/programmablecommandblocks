package com.zeitheron.progcommblocks.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileFilter;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.utils.UV;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.progcommblocks.ProgCommBlocks;
import com.zeitheron.progcommblocks.blocks.tile.TilePCB;
import com.zeitheron.progcommblocks.client.uv.UVDependentPCBCommand;
import com.zeitheron.progcommblocks.client.uv.UVIndependentPCBCommand;
import com.zeitheron.progcommblocks.gui.btns.GuiDownload;
import com.zeitheron.progcommblocks.gui.btns.GuiPastebinUpload;
import com.zeitheron.progcommblocks.gui.btns.GuiSave;
import com.zeitheron.progcommblocks.logic.JFileChooserAlwaysOnTop;
import com.zeitheron.progcommblocks.logic.PCBCommand;
import com.zeitheron.progcommblocks.logic.PCBCommandList;
import com.zeitheron.progcommblocks.network.PacketAddPCBCommand;
import com.zeitheron.progcommblocks.network.PacketRemovePCBCommand;
import com.zeitheron.progcommblocks.network.PacketSetPCBList;
import com.zeitheron.progcommblocks.network.PacketSetPCB_XY;
import com.zeitheron.progcommblocks.network.pastebin.PacketUploadToPastebinRequest;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class GuiPCB extends GuiScreen
{
	public static final ResourceLocation gui = new ResourceLocation("progcommblocks", "textures/gui/pcb.png");
	public static final UV BACKGROUND = new UV(gui, 0.0, 0.0, 256.0, 235.0);
	public static final UV DELETE = new UV(gui, 0.0, 236.0, 20.0, 20.0);
	public static final UV EDIT = new UV(gui, 21.0, 236.0, 20.0, 20.0);
	public static final UV ADD_INDEPENDENT = new UV(gui, 42.0, 236.0, 20.0, 20.0);
	public static final UV ADD_CHILD = new UV(gui, 63.0, 236.0, 20.0, 20.0);
	public static final UV INDEPENDENT_ELEM = new UVIndependentPCBCommand();
	public static final UV CHILD_ELEM = new UVDependentPCBCommand();
	public static final UV HOVER_ELEM = new UV(gui, 126.0, 236.0, 20.0, 20.0);
	public static final UV PASTEBIN = new UV(new ResourceLocation("progcommblocks", "textures/icons/pastebin_upload.png"), 0.0, 0.0, 256.0, 256.0);
	public static final UV LOAD = new UV(new ResourceLocation("progcommblocks", "textures/icons/load.png"), 0.0, 0.0, 256.0, 256.0);
	public static final UV DOWNLOAD = new UV(new ResourceLocation("progcommblocks", "textures/icons/download.png"), 0.0, 0.0, 256.0, 256.0);
	public static final UV SAVE = new UV(new ResourceLocation("progcommblocks", "textures/icons/save.png"), 0.0, 0.0, 256.0, 256.0);
	protected int xSize;
	protected int ySize;
	protected int guiLeft;
	protected int guiTop;
	public final TilePCB tile;
	public PCBCommandList pcb;
	public final List<PCBCommand> commandQueue = new ArrayList<PCBCommand>();
	public int grabX;
	public int grabY;
	public int grabMX;
	public int grabMY;
	public boolean mouseGrabbed = false;
	
	public GuiPCB(TilePCB tile, PCBCommandList pcb)
	{
		this.tile = tile;
		this.pcb = pcb;
		this.xSize = 256;
		this.ySize = 235;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		this.mouseGrabbed = false;
		this.tile.isGuiOpen = true;
		this.commandQueue.addAll(this.pcb.getCommands());
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
		this.grabX = this.tile.guiX;
		this.grabY = this.tile.guiY;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		try
		{
			this.drawDefaultBackground();
			this.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
		} catch(Throwable throwable)
		{
			// empty catch block
		}
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	@Override
	public void onGuiClosed()
	{
		super.onGuiClosed();
		this.tile.isGuiOpen = false;
		PacketSetPCB_XY xy = new PacketSetPCB_XY();
		xy.world = this.tile.getWorld().provider.getDimension();
		xy.pos = this.tile.getPos();
		xy.x = this.tile.guiX;
		xy.y = this.tile.guiY;
		HCNet.INSTANCE.sendToServer(xy);
	}
	
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		int tileX;
		int tileY;
		UV u;
		GL11.glEnable(3042);
		this.tile.isGuiOpen = true;
		BACKGROUND.render(this.guiLeft, this.guiTop);
		int yoff = (this.ySize - 128) / 2;
		PASTEBIN.render(this.guiLeft - 32, this.guiTop + yoff, 32.0, 32.0);
		SAVE.render(this.guiLeft - 32, this.guiTop + 32 + yoff, 32.0, 32.0);
		DOWNLOAD.render(this.guiLeft - 32, this.guiTop + 64 + yoff, 32.0, 32.0);
		LOAD.render(this.guiLeft - 32, this.guiTop + 96 + yoff, 32.0, 32.0);
		ScaledResolution sr = new ScaledResolution(this.mc);
		GL11.glEnable(3089);
		GL11.glScissor((this.guiLeft + 6) * sr.getScaleFactor(), (this.guiTop + 7) * sr.getScaleFactor(), (this.xSize - 12) * sr.getScaleFactor(), (this.ySize - 12) * sr.getScaleFactor());
		boolean drawHoverElem = true;
		try
		{
			for(int i0 = 0; i0 < this.commandQueue.size(); ++i0)
			{
				PCBCommand cmd = this.commandQueue.get(i0);
				if(cmd == null)
					continue;
				u = cmd.parent != null ? CHILD_ELEM : INDEPENDENT_ELEM;
				tileX = cmd.x * 20 - this.grabX;
				tileY = cmd.y * 20 - this.grabY;
				GL11.glPushMatrix();
				GL11.glTranslatef(0.0f, 0.0f, 5.0f);
				u.render(this.guiLeft + tileX + 6, this.guiTop + tileY + 6);
				GL11.glPopMatrix();
				if(mouseX >= this.guiLeft + tileX + 6 && mouseY >= this.guiTop + tileY + 6 && mouseX < this.guiLeft + tileX + 26 && mouseY < this.guiTop + tileY + 26)
				{
					GL11.glPushMatrix();
					GL11.glTranslatef(0.0f, 0.0f, 5.0f);
					if(ProgCommBlocks.proxy.isSHIFTPressed())
					{
						DELETE.render(this.guiLeft + tileX + 6, this.guiTop + tileY + 6);
					} else
					{
						EDIT.render(this.guiLeft + tileX + 6, this.guiTop + tileY + 6);
					}
					GL11.glPopMatrix();
					drawHoverElem = false;
				}
				if(cmd.parent == null)
					continue;
				int ntileX = cmd.parent.x * 20 - this.grabX;
				int ntileY = cmd.parent.y * 20 - this.grabY;
				GL11.glPushMatrix();
				GL11.glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
				GL11.glTranslated(this.guiLeft + 16, this.guiTop + 16, 0.0);
				GL11.glBegin(1);
				GL11.glVertex3d(ntileX, ntileY, 0.0);
				GL11.glVertex3d(tileX, tileY, 0.0);
				GL11.glEnd();
				GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
				GL11.glPopMatrix();
			}
		} catch(Throwable i0)
		{
			// empty catch block
		}
		if(drawHoverElem)
		{
			int mx = mouseX - this.guiLeft - 6 + this.grabX % 20;
			int my = mouseY - this.guiTop - 6 + this.grabY % 20;
			mx /= 20;
			my /= 20;
			tileX = this.guiLeft + 6 + (mx *= 20);
			tileY = this.guiTop + 6 + (my *= 20);
			u = HOVER_ELEM;
			if(ProgCommBlocks.proxy.isSHIFTPressed())
			{
				u = ADD_INDEPENDENT;
				if(Keyboard.isKeyDown(29) || Keyboard.isKeyDown(157))
				{
					u = ADD_CHILD;
				}
			}
			u.render(tileX - this.grabX % 20, tileY - this.grabY % 20);
		}
		GL11.glDisable(3089);
		GL11.glPushMatrix();
		GL11.glTranslatef(0.0f, 0.0f, 10.0f);
		this.drawCenteredString(this.fontRenderer, "X: " + this.grabX + "; Y: " + this.grabY, this.guiLeft + this.xSize / 2, this.guiTop + 6, 16777215);
		GL11.glPopMatrix();
		GL11.glDisable(3042);
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		this.updateGrab(mouseX, mouseY);
		int yoff = (this.ySize - 128) / 2;
		if(mouseX >= this.guiLeft - 32 && mouseY >= this.guiTop + yoff && mouseX < this.guiLeft && mouseY < this.guiTop + 32 + yoff)
		{
			this.mc.displayGuiScreen(new GuiPastebinUpload(this));
			return;
		}
		if(mouseX >= this.guiLeft - 32 && mouseY >= this.guiTop + 32 + yoff && mouseX < this.guiLeft && mouseY < this.guiTop + 64 + yoff)
		{
			GuiSave save = new GuiSave(this);
			this.mc.displayGuiScreen(save);
			PacketUploadToPastebinRequest xy = new PacketUploadToPastebinRequest();
			xy.world = this.tile.getWorld().provider.getDimension();
			xy.pos = this.tile.getPos();
			HCNet.INSTANCE.sendToServer(xy);
			JFileChooserAlwaysOnTop chooser = new JFileChooserAlwaysOnTop();
			chooser.setDialogTitle("Save this PCB");
			chooser.setDialogType(1);
			save.fileChooser = chooser;
			int approved = chooser.showDialog(null, "Save");
			if(approved == 0)
			{
				File sel;
				String selstr = chooser.getSelectedFile().getAbsolutePath();
				if(!selstr.endsWith(".pcb"))
				{
					selstr = selstr + ".pcb";
				}
				save.saveFile = sel = new File(selstr);
			}
			save.fileChooser = null;
			save.save();
			return;
		}
		if(mouseX >= this.guiLeft - 32 && mouseY >= this.guiTop + 64 + yoff && mouseX < this.guiLeft && mouseY < this.guiTop + 96 + yoff)
		{
			this.mc.displayGuiScreen(new GuiDownload(this));
			this.mouseGrabbed = false;
			return;
		}
		if(mouseX >= this.guiLeft - 32 && mouseY >= this.guiTop + 96 + yoff && mouseX < this.guiLeft && mouseY < this.guiTop + 128 + yoff)
		{
			JFileChooserAlwaysOnTop chooser = new JFileChooserAlwaysOnTop();
			chooser.setDialogTitle("Load PCB");
			chooser.setDialogType(0);
			chooser.setAcceptAllFileFilterUsed(false);
			chooser.setFileFilter(new FileFilter()
			{
				
				@Override
				public boolean accept(File f)
				{
					return f.getAbsolutePath().endsWith(".pcb") || f.isDirectory();
				}
				
				@Override
				public String getDescription()
				{
					return "PCB Files";
				}
			});
			int approved = chooser.showDialog(null, "Load");
			if(approved == 0)
			{
				try
				{
					FileInputStream fis = new FileInputStream(chooser.getSelectedFile());
					NBTTagCompound nbt = CompressedStreamTools.readCompressed(fis);
					fis.close();
					PacketSetPCBList xy = new PacketSetPCBList();
					xy.world = this.tile.getWorld().provider.getDimension();
					xy.pos = this.tile.getPos();
					xy.cmd = new PCBCommandList();
					xy.cmd.readFromNBT(nbt);
					HCNet.INSTANCE.sendToServer(xy);
				} catch(Throwable fis)
				{
					// empty catch block
				}
			}
			return;
		}
		boolean drawHoverElem = true;
		try
		{
			for(int i = 0; i < this.commandQueue.size(); ++i)
			{
				PCBCommand cmd = this.commandQueue.get(i);
				if(cmd == null)
					continue;
				int tileX = cmd.x * 20 - this.grabX;
				int tileY = cmd.y * 20 - this.grabY;
				if(mouseX < this.guiLeft + tileX + 6 || mouseY < this.guiTop + tileY + 6 || mouseX >= this.guiLeft + tileX + 26 || mouseY >= this.guiTop + tileY + 26)
					continue;
				drawHoverElem = false;
				if(ProgCommBlocks.proxy.isSHIFTPressed())
				{
					int mx = mouseX - this.guiLeft - 6 + this.grabX;
					int my = mouseY - this.guiTop - 6 + this.grabY;
					this.pcb.remove(mx /= 20, my /= 20);
					PacketRemovePCBCommand xy = new PacketRemovePCBCommand();
					xy.world = this.tile.getWorld().provider.getDimension();
					xy.pos = this.tile.getPos();
					xy.x = mx;
					xy.y = my;
					HCNet.INSTANCE.sendToServer(xy);
					continue;
				}
				this.mouseGrabbed = false;
				this.mc.displayGuiScreen(new GuiEditPCB(cmd, this));
			}
		} catch(Throwable i)
		{
			// empty catch block
		}
		if(drawHoverElem)
		{
			int mx = mouseX - this.guiLeft - 6 + this.grabX;
			int my = mouseY - this.guiTop - 6 + this.grabY;
			mx /= 20;
			my /= 20;
			if(ProgCommBlocks.proxy.isSHIFTPressed())
			{
				if(Keyboard.isKeyDown(29) || Keyboard.isKeyDown(157))
				{
					GuiAddDepPCB gui = new GuiAddDepPCB(this.tile, this.pcb, this);
					gui.posX = mx;
					gui.posY = my;
					this.mc.displayGuiScreen(gui);
				} else
				{
					PCBCommand cmd = new PCBCommand(this.pcb, "");
					cmd.x = mx;
					cmd.y = my;
					PacketAddPCBCommand xy = new PacketAddPCBCommand();
					xy.world = this.tile.getWorld().provider.getDimension();
					xy.pos = this.tile.getPos();
					xy.cmd = cmd;
					HCNet.INSTANCE.sendToServer(xy);
				}
			}
		}
	}
	
	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick)
	{
		super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
		this.updateGrab(mouseX, mouseY);
	}
	
	@Override
	protected void mouseReleased(int mouseX, int mouseY, int state)
	{
		super.mouseReleased(mouseX, mouseY, state);
		this.mouseGrabbed = false;
		this.tile.guiX = this.grabX;
		this.tile.guiY = this.grabY;
	}
	
	public void updateGrab(int x, int y)
	{
		if(x < this.guiLeft + 6 || y < this.guiTop + 6 || x >= this.guiLeft + this.xSize - 6 || y >= this.guiTop + this.ySize - 6)
		{
			return;
		}
		if(!this.mouseGrabbed)
		{
			this.grabX = this.tile.guiX;
			this.grabY = this.tile.guiY;
			this.grabMX = x;
			this.grabMY = y;
			this.mouseGrabbed = true;
		}
		int delX = this.grabMX - x;
		int delY = this.grabMY - y;
		this.grabMX = x;
		this.grabMY = y;
		this.grabX += delX;
		this.grabY += delY;
		this.grabX = Math.max(0, this.grabX);
		this.grabY = Math.max(0, this.grabY);
	}
	
	public static void drawLine(int left, int top, int right, int bottom, int color)
	{
		float f3 = (color >> 24 & 255) / 255.0f;
		float f = (color >> 16 & 255) / 255.0f;
		float f1 = (color >> 8 & 255) / 255.0f;
		float f2 = (color & 255) / 255.0f;
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder renderer = tessellator.getBuffer();
		GlStateManager.enableBlend();
		GlStateManager.disableTexture2D();
		GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
		GlStateManager.color(f, f1, f2, f3);
		GlStateManager.pushMatrix();
		GlStateManager.pushAttrib();
		renderer.begin(1, DefaultVertexFormats.POSITION);
		renderer.pos(left, top, 0.0).color(f, f1, f2, f3).endVertex();
		renderer.pos(right, bottom, 0.0).color(f, f1, f2, f3).endVertex();
		Tessellator.getInstance().draw();
		GlStateManager.popMatrix();
		GlStateManager.popAttrib();
		GlStateManager.enableTexture2D();
		GlStateManager.disableBlend();
	}
}
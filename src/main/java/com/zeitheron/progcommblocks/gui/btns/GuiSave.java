/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes: net.minecraft.client.Minecraft
 * net.minecraft.client.gui.GuiScreen net.minecraft.nbt.CompressedStreamTools
 * net.minecraft.nbt.NBTTagCompound */
package com.zeitheron.progcommblocks.gui.btns;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.zeitheron.progcommblocks.gui.GuiPCB;
import com.zeitheron.progcommblocks.logic.PCBCommandList;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;

public class GuiSave extends GuiScreen
{
	protected int xSize;
	protected int ySize;
	protected int guiLeft;
	protected int guiTop;
	public Object fileChooser;
	public PCBCommandList list;
	public File saveFile;
	public final GuiPCB parent;
	
	public GuiSave(GuiPCB parent)
	{
		this.parent = parent;
		this.xSize = 200;
		this.ySize = 60;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.drawDefaultBackground();
		this.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		if(this.saveFile == null || this.list == null)
		{
			this.mc.displayGuiScreen(this.parent);
			this.parent.mouseGrabbed = false;
			return;
		}
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
	}
	
	public void save()
	{
		while(this.list == null || this.saveFile == null && this.fileChooser != null)
		{
			try
			{
				Thread.sleep(5000);
			} catch(Throwable throwable)
			{
			}
		}
		if(this.saveFile == null || this.list == null)
		{
			return;
		}
		try
		{
			FileOutputStream fos = new FileOutputStream(this.saveFile);
			NBTTagCompound nbt = new NBTTagCompound();
			this.list.writeToNBT(nbt);
			CompressedStreamTools.writeCompressed(nbt, fos);
			fos.close();
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
		this.mc.displayGuiScreen(this.parent);
		this.parent.mouseGrabbed = false;
	}
}

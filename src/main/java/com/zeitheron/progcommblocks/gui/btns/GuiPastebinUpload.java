package com.zeitheron.progcommblocks.gui.btns;

import java.io.IOException;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.progcommblocks.gui.GuiPCB;
import com.zeitheron.progcommblocks.network.pastebin.PacketUploadToPastebinRequest;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

public class GuiPastebinUpload extends GuiScreen
{
	protected int xSize;
	protected int ySize;
	protected int guiLeft;
	protected int guiTop;
	public GuiTextField command;
	public final GuiPCB parent;
	
	public GuiPastebinUpload(GuiPCB parent)
	{
		this.parent = parent;
		this.xSize = 200;
		this.ySize = 60;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
		this.command = new GuiTextField(0, this.fontRenderer, this.guiLeft, this.guiTop, 200, 20);
		this.command.setMaxStringLength(512);
		this.addButton(new GuiButton(0, this.guiLeft, this.guiTop + 40, 40, 20, "Upload"));
		this.addButton(new GuiButton(1, this.guiLeft + 160, this.guiTop + 40, 40, 20, "Cancel"));
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.drawDefaultBackground();
		this.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		this.command.drawTextBox();
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		this.command.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(!this.command.textboxKeyTyped(typedChar, keyCode) && keyCode == 1)
		{
			this.mc.displayGuiScreen(this.parent);
			this.parent.mouseGrabbed = false;
		}
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 0)
		{
			PacketUploadToPastebinRequest xy = new PacketUploadToPastebinRequest();
			xy.world = this.parent.tile.getWorld().provider.getDimension();
			xy.pos = this.parent.tile.getPos();
			this.command.setText("<waiting for pastebin link>");
			HCNet.INSTANCE.sendToServer(xy);
		}
		if(button.id == 1)
		{
			this.mc.displayGuiScreen(this.parent);
			this.parent.mouseGrabbed = false;
		}
	}
}

package com.zeitheron.progcommblocks.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.utils.UV;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.progcommblocks.blocks.tile.TilePCB;
import com.zeitheron.progcommblocks.logic.PCBCommand;
import com.zeitheron.progcommblocks.logic.PCBCommandList;
import com.zeitheron.progcommblocks.network.PacketAddDepPCBCommand;
import com.zeitheron.progcommblocks.network.PacketSetPCB_XY;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.ResourceLocation;

public class GuiAddDepPCB extends GuiScreen
{
	public static final ResourceLocation gui = new ResourceLocation("progcommblocks", "textures/gui/pcb.png");
	protected int xSize;
	protected int ySize;
	protected int guiLeft;
	protected int guiTop;
	protected int posX;
	protected int posY;
	public final TilePCB tile;
	public PCBCommandList pcb;
	public final List<PCBCommand> commandQueue = new ArrayList<PCBCommand>();
	public int grabX;
	public int grabY;
	public int grabMX;
	public int grabMY;
	private GuiPCB parentGui;
	public boolean mouseGrabbed = false;
	
	public GuiAddDepPCB(TilePCB tile, PCBCommandList pcb, GuiPCB parentGui)
	{
		this.tile = tile;
		this.parentGui = parentGui;
		this.pcb = pcb;
		this.xSize = 256;
		this.ySize = 235;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		this.mouseGrabbed = false;
		this.tile.isGuiOpen = true;
		this.commandQueue.addAll(this.pcb.getCommands());
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
		this.grabX = this.tile.guiX;
		this.grabY = this.tile.guiY;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		try
		{
			this.drawDefaultBackground();
			this.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
		} catch(Throwable throwable)
		{
			// empty catch block
		}
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	@Override
	public void onGuiClosed()
	{
		super.onGuiClosed();
		this.tile.isGuiOpen = false;
		PacketSetPCB_XY xy = new PacketSetPCB_XY();
		xy.world = this.tile.getWorld().provider.getDimension();
		xy.pos = this.tile.getPos();
		xy.x = this.tile.guiX;
		xy.y = this.tile.guiY;
		HCNet.INSTANCE.sendToServer(xy);
	}
	
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		int tileY;
		int tileX;
		GL11.glEnable(3042);
		this.tile.isGuiOpen = true;
		GuiPCB.BACKGROUND.render(this.guiLeft, this.guiTop);
		ScaledResolution sr = new ScaledResolution(this.mc);
		GL11.glEnable(3089);
		GL11.glScissor((this.guiLeft + 6) * sr.getScaleFactor(), (this.guiTop + 7) * sr.getScaleFactor(), (this.xSize - 12) * sr.getScaleFactor(), (this.ySize - 12) * sr.getScaleFactor());
		boolean renderHover = false;
		try
		{
			for(int i0 = 0; i0 < this.commandQueue.size(); ++i0)
			{
				PCBCommand cmd = this.commandQueue.get(i0);
				if(cmd == null)
					continue;
				UV u = cmd.parent != null ? GuiPCB.CHILD_ELEM : GuiPCB.INDEPENDENT_ELEM;
				tileX = cmd.x * 20 - this.grabX;
				tileY = cmd.y * 20 - this.grabY;
				GL11.glPushMatrix();
				GL11.glTranslatef(0.0f, 0.0f, 5.0f);
				u.render((double) (this.guiLeft + tileX + 6), (double) (this.guiTop + tileY + 6));
				GL11.glPopMatrix();
				if(mouseX >= this.guiLeft + tileX + 6 && mouseY >= this.guiTop + tileY + 6 && mouseX < this.guiLeft + tileX + 26 && mouseY < this.guiTop + tileY + 26)
				{
					renderHover = true;
				}
				if(cmd.parent == null)
					continue;
				int ntileX = cmd.parent.x * 20 - this.grabX;
				int ntileY = cmd.parent.y * 20 - this.grabY;
				GL11.glPushMatrix();
				GL11.glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
				GL11.glTranslated(this.guiLeft + 16, this.guiTop + 16, 0.0);
				GL11.glBegin(1);
				GL11.glVertex3d(ntileX, ntileY, 0.0);
				GL11.glVertex3d(tileX, tileY, 0.0);
				GL11.glEnd();
				GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
				GL11.glPopMatrix();
			}
		} catch(Throwable i0)
		{
			// empty catch block
		}
		if(renderHover)
		{
			GL11.glDisable(3042);
			int mx = mouseX - this.guiLeft - 6 + this.grabX % 20;
			int my = mouseY - this.guiTop - 6 + this.grabY % 20;
			mx /= 20;
			my /= 20;
			tileX = this.guiLeft + 6 + (mx *= 20);
			tileY = this.guiTop + 6 + (my *= 20);
			GuiPCB.HOVER_ELEM.render(tileX - this.grabX % 20, tileY - this.grabY % 20);
			GL11.glEnable(3042);
		}
		GL11.glDisable(3089);
		GL11.glPushMatrix();
		GL11.glTranslatef(0.0f, 0.0f, 10.0f);
		this.drawCenteredString(this.fontRenderer, "X: " + this.grabX + "; Y: " + this.grabY, this.guiLeft + this.xSize / 2, this.guiTop + 6, 16777215);
		GL11.glPopMatrix();
		GL11.glDisable(3042);
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		this.updateGrab(mouseX, mouseY);
		try
		{
			for(int i = 0; i < this.commandQueue.size(); ++i)
			{
				PCBCommand cmd = this.commandQueue.get(i);
				if(cmd == null)
					continue;
				int tileX = cmd.x * 20 - this.grabX;
				int tileY = cmd.y * 20 - this.grabY;
				if(mouseX < this.guiLeft + tileX + 6 || mouseY < this.guiTop + tileY + 6 || mouseX >= this.guiLeft + tileX + 26 || mouseY >= this.guiTop + tileY + 26)
					continue;
				PCBCommand cmd0 = new PCBCommand(this.pcb, "");
				cmd0.x = this.posX;
				cmd0.y = this.posY;
				PacketAddDepPCBCommand xy = new PacketAddDepPCBCommand();
				xy.world = this.tile.getWorld().provider.getDimension();
				xy.pos = this.tile.getPos();
				xy.cmd = cmd0;
				xy.depX = cmd.x;
				xy.depY = cmd.y;
				HCNet.INSTANCE.sendToServer(xy);
				this.mc.displayGuiScreen(new GuiPCBWaiting(this.tile));
				break;
			}
		} catch(Throwable i)
		{
			// empty catch block
		}
	}
	
	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick)
	{
		super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
		this.updateGrab(mouseX, mouseY);
	}
	
	@Override
	protected void mouseReleased(int mouseX, int mouseY, int state)
	{
		super.mouseReleased(mouseX, mouseY, state);
		this.mouseGrabbed = false;
		this.tile.guiX = this.grabX;
		this.tile.guiY = this.grabY;
	}
	
	public void updateGrab(int x, int y)
	{
		if(x < this.guiLeft + 6 || y < this.guiTop + 6 || x >= this.guiLeft + this.xSize - 6 || y >= this.guiTop + this.ySize - 6)
		{
			return;
		}
		if(!this.mouseGrabbed)
		{
			this.grabX = this.tile.guiX;
			this.grabY = this.tile.guiY;
			this.grabMX = x;
			this.grabMY = y;
			this.mouseGrabbed = true;
		}
		int delX = this.grabMX - x;
		int delY = this.grabMY - y;
		this.grabMX = x;
		this.grabMY = y;
		this.grabX += delX;
		this.grabY += delY;
		this.grabX = Math.max(0, this.grabX);
		this.grabY = Math.max(0, this.grabY);
	}
}

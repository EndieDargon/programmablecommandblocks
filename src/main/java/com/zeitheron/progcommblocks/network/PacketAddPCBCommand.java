/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes:
 * com.mrdimka.hammercore.common.utils.WorldUtil
 * com.mrdimka.hammercore.net.HCNetwork
 * com.mrdimka.hammercore.net.packetAPI.IPacket
 * com.mrdimka.hammercore.net.packetAPI.IPacketListener
 * com.mrdimka.hammercore.net.packetAPI.PacketManager net.minecraft.nbt.NBTBase
 * net.minecraft.nbt.NBTTagCompound net.minecraft.tileentity.TileEntity
 * net.minecraft.util.math.BlockPos net.minecraft.world.World
 * net.minecraftforge.fml.common.network.NetworkRegistry
 * net.minecraftforge.fml.common.network.NetworkRegistry$TargetPoint
 * net.minecraftforge.fml.common.network.simpleimpl.MessageContext
 * net.minecraftforge.fml.relauncher.Side */
package com.zeitheron.progcommblocks.network;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.progcommblocks.blocks.tile.TilePCB;
import com.zeitheron.progcommblocks.logic.PCBCommand;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;

public class PacketAddPCBCommand implements IPacket
{
	public int world;
	public PCBCommand cmd;
	public BlockPos pos;
	private NBTTagCompound cmdTag;
	
	static
	{
		IPacket.handle(PacketAddPCBCommand.class, PacketAddPCBCommand::new);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("Dim", this.world);
		nbt.setLong("Pos", this.pos.toLong());
		NBTTagCompound tag = new NBTTagCompound();
		this.cmd.writeToNBT(tag);
		nbt.setTag("Cmd", tag);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.world = nbt.getInteger("Dim");
		this.pos = BlockPos.fromLong(nbt.getLong("Pos"));
		this.cmdTag = nbt.getCompoundTag("Cmd");
	}
	
	@Override
	public IPacket execute(Side side, PacketContext ctx)
	{
		TilePCB pcb;
		World w = WorldUtil.getWorld(ctx, world);
		if(w != null && w.isBlockLoaded(pos) && (pcb = WorldUtil.cast(w.getTileEntity(pos), TilePCB.class)) != null)
		{
			PCBCommand c = new PCBCommand(pcb.commandList, "");
			c.readFromNBT(cmdTag);
			pcb.commandList.addCommand(c);
			if(!w.isRemote)
				pcb.sync();
			if(side == Side.SERVER)
				HCNet.INSTANCE.sendToAllAround(new PacketRefreshGuiPCB2(pcb.commandList), pcb.getSyncPoint(32));
		}
		return null;
	}
}

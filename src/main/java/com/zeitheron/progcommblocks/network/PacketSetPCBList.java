package com.zeitheron.progcommblocks.network;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.progcommblocks.blocks.tile.TilePCB;
import com.zeitheron.progcommblocks.logic.PCBCommandList;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;

public class PacketSetPCBList implements IPacket
{
	public int world;
	public PCBCommandList cmd;
	public BlockPos pos;
	private NBTTagCompound cmdTag;
	
	static
	{
		IPacket.handle(PacketSetPCBList.class, PacketSetPCBList::new);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("Dim", this.world);
		nbt.setLong("Pos", this.pos.toLong());
		NBTTagCompound tag = new NBTTagCompound();
		this.cmd.writeToNBT(tag);
		nbt.setTag("Cmd", tag);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.world = nbt.getInteger("Dim");
		this.pos = BlockPos.fromLong(nbt.getLong("Pos"));
		this.cmdTag = nbt.getCompoundTag("Cmd");
	}
	
	@Override
	public IPacket execute(Side side, PacketContext ctx)
	{
		TilePCB pcb;
		World w = WorldUtil.getWorld(ctx, world);
		if(w != null && w.isBlockLoaded(pos) && (pcb = WorldUtil.cast(w.getTileEntity(pos), TilePCB.class)) != null)
		{
			pcb.commandList.readFromNBT(cmdTag);
			if(!w.isRemote)
				pcb.sync();
			if(side == Side.SERVER)
				HCNet.INSTANCE.sendToAllAround(new PacketRefreshGuiPCB2(pcb.commandList), pcb.getSyncPoint(32));
		}
		return null;
	}
}
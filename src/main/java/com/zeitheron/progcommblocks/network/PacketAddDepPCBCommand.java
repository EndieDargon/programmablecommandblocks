package com.zeitheron.progcommblocks.network;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.progcommblocks.blocks.tile.TilePCB;
import com.zeitheron.progcommblocks.logic.PCBCommand;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;

public class PacketAddDepPCBCommand implements IPacket
{
	public int world;
	public PCBCommand cmd;
	public BlockPos pos;
	private NBTTagCompound cmdTag;
	public int depX;
	public int depY;
	
	static
	{
		IPacket.handle(PacketAddDepPCBCommand.class, PacketAddDepPCBCommand::new);
	}
	
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("Dim", this.world);
		nbt.setLong("Pos", this.pos.toLong());
		NBTTagCompound tag = new NBTTagCompound();
		this.cmd.writeToNBT(tag);
		nbt.setTag("Cmd", tag);
		nbt.setInteger("DepX", this.depX);
		nbt.setInteger("DepY", this.depY);
	}
	
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.world = nbt.getInteger("Dim");
		this.pos = BlockPos.fromLong(nbt.getLong("Pos"));
		this.cmdTag = nbt.getCompoundTag("Cmd");
		this.depX = nbt.getInteger("DepX");
		this.depY = nbt.getInteger("DepY");
	}
	
	@Override
	public IPacket execute(Side side, PacketContext ctx)
	{
		TilePCB pcb;
		World w = WorldUtil.getWorld(ctx, world);
		if(w != null && w.isBlockLoaded(pos) && (pcb = WorldUtil.cast(w.getTileEntity(pos), TilePCB.class)) != null)
		{
			PCBCommand parent = null;
			for(PCBCommand cmd : pcb.commandList.getCommands())
			{
				if(cmd.x != depX || cmd.y != depY)
					continue;
				parent = cmd;
			}
			PCBCommand c = new PCBCommand(parent, "");
			c.readFromNBT(cmdTag);
			if(!w.isRemote)
				pcb.sync();
			if(side == Side.SERVER)
				HCNet.INSTANCE.sendToAllAround(new PacketRefreshGuiPCB2(pcb.commandList), pcb.getSyncPoint(32));
		}
		return null;
	}
}

package com.zeitheron.progcommblocks.network;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.progcommblocks.blocks.tile.TilePCB;
import com.zeitheron.progcommblocks.logic.PCBCommand;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;

public class PacketRemovePCBCommand implements IPacket
{
	public int world;
	public int x;
	public int y;
	public PCBCommand cmd;
	public BlockPos pos;
	private NBTTagCompound cmdTag;
	
	static
	{
		IPacket.handle(PacketRemovePCBCommand.class, PacketRemovePCBCommand::new);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("Dim", this.world);
		nbt.setLong("Pos", this.pos.toLong());
		nbt.setInteger("x", this.x);
		nbt.setInteger("y", this.y);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.world = nbt.getInteger("Dim");
		this.pos = BlockPos.fromLong(nbt.getLong("Pos"));
		this.x = nbt.getInteger("x");
		this.y = nbt.getInteger("y");
	}
	
	@Override
	public IPacket execute(Side side, PacketContext ctx)
	{
		TilePCB pcb;
		World w = WorldUtil.getWorld(ctx, world);
		if(w != null && w.isBlockLoaded(pos) && (pcb = WorldUtil.cast(w.getTileEntity(pos), TilePCB.class)) != null)
		{
			pcb.commandList.remove(x, y);
			if(!w.isRemote)
				pcb.sync();
			if(side == Side.SERVER)
				HCNet.INSTANCE.sendToAllAround(new PacketRefreshGuiPCB2(pcb.commandList), pcb.getSyncPoint(32));
		}
		return null;
	}
}

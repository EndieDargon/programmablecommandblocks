package com.zeitheron.progcommblocks.network.pastebin;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.progcommblocks.gui.btns.GuiPastebinUpload;
import com.zeitheron.progcommblocks.gui.btns.GuiSave;
import com.zeitheron.progcommblocks.logic.PCBIO;
import com.zeitheron.progcommblocks.logic.PCBToJSON;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketUploadedToPastebinResponse implements IPacket
{
	public String message;
	
	static
	{
		IPacket.handle(PacketUploadedToPastebinResponse.class, PacketUploadedToPastebinResponse::new);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString("Msg", this.message);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.message = nbt.getString("Msg");
	}
	
	@SideOnly(value = Side.CLIENT)
	@Override
	public IPacket executeOnClient(PacketContext net)
	{
		GuiScreen gui = Minecraft.getMinecraft().currentScreen;
		if(gui instanceof GuiPastebinUpload)
		{
			new Thread(() ->
			{
				String msg = PCBIO.upload(this.message, "[ProgrammableCommandBlocks] PCB-Generated");
				((GuiPastebinUpload) gui).command.setText("http://pastebin.com/raw/" + msg);
			}).start();
		}
		if(gui instanceof GuiSave)
		{
			new Thread(() ->
			{
				GuiSave save = (GuiSave) gui;
				save.list = PCBToJSON.toPCB(this.message);
			}).start();
		}
		return null;
	}
}

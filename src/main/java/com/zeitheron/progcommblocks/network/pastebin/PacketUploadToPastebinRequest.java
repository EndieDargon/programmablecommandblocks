package com.zeitheron.progcommblocks.network.pastebin;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.progcommblocks.blocks.tile.TilePCB;
import com.zeitheron.progcommblocks.logic.PCBToJSON;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;

public class PacketUploadToPastebinRequest implements IPacket
{
	public int world;
	public BlockPos pos;
	
	static
	{
		IPacket.handle(PacketUploadToPastebinRequest.class, PacketUploadToPastebinRequest::new);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("Dim", this.world);
		nbt.setLong("Pos", this.pos.toLong());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.world = nbt.getInteger("Dim");
		this.pos = BlockPos.fromLong(nbt.getLong("Pos"));
	}
	
	@Override
	public IPacket execute(Side side, PacketContext ctx)
	{
		TilePCB pcb;
		World w = WorldUtil.getWorld(ctx, world);
		if(w != null && w.isBlockLoaded(pos) && (pcb = WorldUtil.cast(w.getTileEntity(pos), TilePCB.class)) != null)
		{
			PacketUploadedToPastebinResponse r = new PacketUploadedToPastebinResponse();
			r.message = PCBToJSON.toNBT(pcb.commandList);
			return r;
		}
		return null;
	}
}

package com.zeitheron.progcommblocks.network;

import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.progcommblocks.blocks.tile.TilePCB;
import com.zeitheron.progcommblocks.gui.GuiPCB;
import com.zeitheron.progcommblocks.gui.GuiPCBWaiting;
import com.zeitheron.progcommblocks.logic.PCBCommandList;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketRefreshGuiPCB2 implements IPacket
{
	public PCBCommandList list;
	public int world;
	public BlockPos pos;
	
	static
	{
		IPacket.handle(PacketRefreshGuiPCB2.class, PacketRefreshGuiPCB2::new);
	}
	
	public PacketRefreshGuiPCB2()
	{
	}
	
	public PacketRefreshGuiPCB2(PCBCommandList list)
	{
		this.list = list;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("Dim", this.world);
		if(this.pos != null)
		{
			nbt.setLong("Pos", this.pos.toLong());
		}
		if(this.list != null)
		{
			this.list.writeToNBT(nbt);
		}
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		this.world = nbt.getInteger("Dim");
		this.pos = BlockPos.fromLong(nbt.getLong("Pos"));
		if(nbt.getKeySet().size() > 0)
		{
			this.list = new PCBCommandList();
			this.list.readFromNBT(nbt);
		}
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		World w = WorldUtil.getWorld(net, world);
		if(w != null && w.isBlockLoaded(pos))
		{
			TilePCB pcb = WorldUtil.cast(w.getTileEntity(pos), TilePCB.class);
			list = pcb.commandList;
			if(!w.isRemote)
				pcb.sync();
		}
		return this;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IPacket executeOnClient(PacketContext net)
	{
		GuiScreen gui = Minecraft.getMinecraft().currentScreen;
		if(gui instanceof GuiPCBWaiting)
			Minecraft.getMinecraft().displayGuiScreen(new GuiPCB(((GuiPCBWaiting) gui).pcb, this.list));
		if(gui instanceof GuiPCB)
		{
			((GuiPCB) gui).pcb = this.list;
			((GuiPCB) gui).commandQueue.clear();
			((GuiPCB) gui).commandQueue.addAll(((GuiPCB) gui).pcb.getCommands());
		}
		
		return null;
	}
}
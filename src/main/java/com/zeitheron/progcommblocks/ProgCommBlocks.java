package com.zeitheron.progcommblocks;

import javax.swing.UIManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import com.zeitheron.progcommblocks.commands.CommandCalculate;
import com.zeitheron.progcommblocks.commands.CommandCut;
import com.zeitheron.progcommblocks.commands.CommandGetPos;
import com.zeitheron.progcommblocks.commands.CommandIsOfflineWorld;
import com.zeitheron.progcommblocks.init.ModBlocks;
import com.zeitheron.progcommblocks.proxy.CommonProxy;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

@Mod(modid = ProgCommBlocks.MOD_ID, name = "Programmable Command Blocks", version = "@VERSION@", dependencies = "required-after:hammercore", certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97")
public class ProgCommBlocks
{
	public static final String MOD_ID = "progcommblocks";
	
	@Instance
	public static ProgCommBlocks instance;
	
	@SidedProxy(clientSide = "com.zeitheron.progcommblocks.proxy.ClientProxy", serverSide = "com.zeitheron.progcommblocks.proxy.CommonProxy")
	public static CommonProxy proxy;
	
	private static final Logger LOG = LogManager.getLogger(MOD_ID);
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent evt)
	{
		SimpleRegistration.registerFieldBlocksFrom(ModBlocks.class, MOD_ID, null);
		
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Throwable throwable)
		{
		}
	}
	
	@EventHandler
	public void init(FMLPreInitializationEvent evt)
	{
	}
	
	@EventHandler
	public void registerCommands(FMLServerStartingEvent evt)
	{
		evt.registerServerCommand(new CommandGetPos());
		evt.registerServerCommand(new CommandCalculate());
		evt.registerServerCommand(new CommandCut());
		evt.registerServerCommand(new CommandIsOfflineWorld());
	}
	
	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with ProgrammableCommandBlocks jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://minecraft.curseforge.com/projects/262071 !");
		LOG.warn("*****************************");
		HammerCore.invalidCertificates.put(MOD_ID, "https://minecraft.curseforge.com/projects/262071");
	}
}
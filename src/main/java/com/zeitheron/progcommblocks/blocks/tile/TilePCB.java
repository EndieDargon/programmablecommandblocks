package com.zeitheron.progcommblocks.blocks.tile;

import javax.annotation.Nullable;

import com.zeitheron.hammercore.client.gui.impl.container.ContainerEmpty;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.progcommblocks.gui.GuiPCBWaiting;
import com.zeitheron.progcommblocks.logic.PCBCommandList;
import com.zeitheron.progcommblocks.network.PacketSetPCBList;

import io.netty.buffer.ByteBuf;
import net.minecraft.command.CommandResultStats;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.CommandBlockBaseLogic;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TilePCB extends TileSyncableTickable
{
	public boolean isGuiOpen = false;
	public int guiX;
	public int guiY;
	public boolean syncDirty = false;
	private boolean powered;
	private boolean sendToClient;
	public final PCBCommandList commandList = new PCBCommandList();
	private final CommandBlockBaseLogic commandBlockLogic;
	
	public TilePCB()
	{
		this.commandBlockLogic = new CommandBlockBaseLogic()
		{
			@Override
			public BlockPos getPosition()
			{
				return TilePCB.this.pos;
			}
			
			@Override
			public Vec3d getPositionVector()
			{
				return new Vec3d(TilePCB.this.pos.getX() + 0.5, TilePCB.this.pos.getY() + 0.5, TilePCB.this.pos.getZ() + 0.5);
			}
			
			@Override
			public World getEntityWorld()
			{
				return TilePCB.this.getWorld();
			}
			
			@Override
			public void setCommand(String command)
			{
				TilePCB.this.sync();
			}
			
			@Override
			public void updateCommand()
			{
				TilePCB.this.sync();
			}
			
			@Override
			@SideOnly(value = Side.CLIENT)
			public int getCommandBlockType()
			{
				return 0;
			}
			
			@Override
			public boolean canUseCommand(int permLevel, String commandName)
			{
				return true;
			}
			
			@Override
			@SideOnly(value = Side.CLIENT)
			public void fillInInfo(ByteBuf buf)
			{
				buf.writeInt(TilePCB.this.pos.getX());
				buf.writeInt(TilePCB.this.pos.getY());
				buf.writeInt(TilePCB.this.pos.getZ());
			}
			
			@Override
			public Entity getCommandSenderEntity()
			{
				return null;
			}
			
			@Override
			public MinecraftServer getServer()
			{
				return TilePCB.this.world.getMinecraftServer();
			}
		};
	}
	
	@Override
	public void tick()
	{
		boolean powered_;
		boolean bl = powered_ = this.world.isBlockIndirectlyGettingPowered(this.pos) > 0;
		if(this.powered != powered_ && !this.world.isRemote)
		{
			this.powered = powered_;
			if(this.powered)
			{
				try
				{
					this.syncDirty = true;
					this.commandList.execute(this.commandBlockLogic);
				} catch(Throwable throwable)
				{
					// empty catch block
				}
			}
		}
		if(this.syncDirty)
		{
			this.syncDirty = false;
			if(!this.world.isRemote)
			{
				this.sync();
				PacketSetPCBList list = new PacketSetPCBList();
				list.pos = this.pos;
				list.world = this.world.provider.getDimension();
				list.cmd = this.commandList;
				HCNet.INSTANCE.sendToAllAround(list, this.getSyncPoint(256));
			}
		}
	}
	
	@Override
	public void writeNBT(NBTTagCompound compound)
	{
		compound.setBoolean("powered", this.isPowered());
		this.commandList.writeToNBT(compound);
		compound.setInteger("guiX", this.guiX);
		compound.setInteger("guiY", this.guiY);
	}
	
	@Override
	public void readNBT(NBTTagCompound compound)
	{
		this.setPowered(compound.getBoolean("powered"));
		this.commandList.readFromNBT(compound);
		this.guiX = compound.getInteger("guiX");
		this.guiY = compound.getInteger("guiY");
	}
	
	@Override
	@Nullable
	public SPacketUpdateTileEntity getUpdatePacket()
	{
		if(this.isSendToClient())
		{
			this.setSendToClient(false);
			NBTTagCompound nbttagcompound = this.writeToNBT(new NBTTagCompound());
			return new SPacketUpdateTileEntity(this.pos, 2, nbttagcompound);
		}
		return null;
	}
	
	@Override
	public boolean onlyOpsCanSetNbt()
	{
		return true;
	}
	
	public CommandBlockBaseLogic getCommandBlockLogic()
	{
		return this.commandBlockLogic;
	}
	
	public CommandResultStats getCommandResultStats()
	{
		return this.commandBlockLogic.getCommandResultStats();
	}
	
	public void setPowered(boolean poweredIn)
	{
		this.powered = poweredIn;
	}
	
	public boolean isPowered()
	{
		return this.powered;
	}
	
	public boolean isSendToClient()
	{
		return this.sendToClient;
	}
	
	public void setSendToClient(boolean sendToClient)
	{
		this.sendToClient = sendToClient;
	}
	
	@Override
	public boolean hasGui()
	{
		return super.hasGui();
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiPCBWaiting(this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerEmpty();
	}
}
package com.zeitheron.progcommblocks.blocks;

import java.util.List;

import com.zeitheron.hammercore.utils.ChatUtil;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.progcommblocks.ProgCommBlocks;
import com.zeitheron.progcommblocks.blocks.tile.TilePCB;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDirectional;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockPCB extends Block implements ITileEntityProvider
{
	public static final PropertyDirection FACING = BlockDirectional.FACING;
	
	public BlockPCB()
	{
		super(Material.ROCK);
		this.setUnlocalizedName("pcb");
		this.setHardness(-1.0f);
		this.setResistance(Float.MAX_VALUE);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(!worldIn.isRemote)
		{
			TilePCB pcb = WorldUtil.cast(worldIn.getTileEntity(pos), TilePCB.class);
			if(pcb == null)
			{
				return false;
			}
			if(playerIn.capabilities.isCreativeMode && playerIn.canUseCommand(4, ""))
			{
				pcb.tryOpenGui(playerIn, worldIn);
			} else
			{
				ChatUtil.sendNoSpam(playerIn, new ITextComponent[] { new TextComponentTranslation("chat.progcommblocks:pcb_no_permission", new Object[0]) });
				return false;
			}
		}
		return true;
	}
	
	@Override
	public boolean hasComparatorInputOverride(IBlockState state)
	{
		return true;
	}
	
	@Override
	public int getComparatorInputOverride(IBlockState blockState, World worldIn, BlockPos pos)
	{
		TileEntity tileentity = worldIn.getTileEntity(pos);
		return tileentity instanceof TilePCB ? ((TilePCB) tileentity).getCommandBlockLogic().getSuccessCount() : 0;
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TilePCB();
	}
	
	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty((IProperty) FACING, (Comparable) EnumFacing.getFront(meta & 7));
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		return ((EnumFacing) state.getValue((IProperty) FACING)).getIndex();
	}
	
	@Override
	public IBlockState withRotation(IBlockState state, Rotation rot)
	{
		return state.withProperty((IProperty) FACING, (Comparable) rot.rotate((EnumFacing) state.getValue((IProperty) FACING)));
	}
	
	@Override
	public IBlockState withMirror(IBlockState state, Mirror mirrorIn)
	{
		return state.withRotation(mirrorIn.toRotation((EnumFacing) state.getValue((IProperty) FACING)));
	}
	
	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, new IProperty[] { FACING });
	}
	
	@Override
	public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		return this.getDefaultState().withProperty((IProperty) FACING, (Comparable) EnumFacing.getDirectionFromEntityLiving(pos, placer));
	}
	
	@Override
	public int tickRate(World worldIn)
	{
		return 1;
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, World player, List<String> tooltip, ITooltipFlag advanced)
	{
		tooltip.add(I18n.format(getUnlocalizedName() + (ProgCommBlocks.proxy.isSHIFTPressed() ? "_shift" : "") + ".name"));
	}
}
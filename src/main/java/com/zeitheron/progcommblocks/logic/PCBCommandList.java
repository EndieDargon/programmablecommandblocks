/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes: net.minecraft.nbt.NBTBase
 * net.minecraft.nbt.NBTTagCompound net.minecraft.nbt.NBTTagList
 * net.minecraft.tileentity.CommandBlockBaseLogic */
package com.zeitheron.progcommblocks.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.CommandBlockBaseLogic;

public class PCBCommandList
{
	public final Map<String, String> vars = new HashMap<String, String>();
	private final Map<Integer, Map<Integer, Integer>> cmdpos = new HashMap<Integer, Map<Integer, Integer>>();
	private final List<PCBCommand> cmds = new ArrayList<PCBCommand>();
	private final Map<PCBCommand, int[]> cmdposrev = new HashMap<PCBCommand, int[]>();
	
	public void addCommand(PCBCommand cmd)
	{
		this.addCommand(cmd.x, cmd.y, cmd);
	}
	
	public void addCommand(int x, int y, PCBCommand cmd)
	{
		int i = this.cmds.size();
		this.cmds.add(cmd);
		Map<Integer, Integer> map = this.cmdpos.get(x);
		if(map == null)
		{
			map = new HashMap<Integer, Integer>();
			this.cmdpos.put(x, map);
		}
		map.put(y, i);
		this.cmdposrev.put(cmd, new int[] { x, y });
		cmd.x = x;
		cmd.y = y;
	}
	
	public void remove(int x, int y)
	{
		for(PCBCommand cmd : new ArrayList<PCBCommand>(this.cmds))
		{
			if(cmd.x == x && cmd.y == y)
			{
				this.cmds.remove(cmd);
				Map<Integer, Integer> map = this.cmdpos.get(x);
				if(map == null)
				{
					map = new HashMap<Integer, Integer>();
					this.cmdpos.put(x, map);
				}
				map.remove(y);
				this.cmdposrev.remove(cmd);
				continue;
			}
			this.tryRemove(cmd, x, y);
		}
	}
	
	private void tryRemove(PCBCommand cmd, int x, int y)
	{
		PCBCommand del = null;
		for(PCBCommand child : cmd.childs)
		{
			if(child.x != x || child.y != y)
				continue;
			del = child;
		}
		if(del != null)
		{
			cmd.childs.remove(del);
			return;
		}
		for(PCBCommand child : cmd.childs)
		{
			this.tryRemove(child, x, y);
		}
	}
	
	public void setCommand(int x, int y, String cmd_)
	{
		for(PCBCommand cmd : this.getCommands())
		{
			if(cmd.x != x || cmd.y != y)
				continue;
			cmd.command = cmd_;
			break;
		}
	}
	
	private void trySetCMD(PCBCommand cmd, int x, int y, String cmd_)
	{
		PCBCommand del = null;
		for(PCBCommand child : cmd.childs)
		{
			if(child.x != x || child.y != y)
				continue;
			del = child;
		}
		if(del != null)
		{
			del.command = cmd_;
		}
		for(PCBCommand child : cmd.childs)
		{
			this.tryRemove(child, x, y);
		}
	}
	
	public int getCommandX(PCBCommand cmd)
	{
		return this.cmdposrev.get(cmd) != null ? this.cmdposrev.get(cmd)[0] : 0;
	}
	
	public int getCommandY(PCBCommand cmd)
	{
		return this.cmdposrev.get(cmd) != null ? this.cmdposrev.get(cmd)[1] : 0;
	}
	
	public void execute(CommandBlockBaseLogic logic)
	{
		for(PCBCommand cmd : this.cmds)
		{
			try
			{
				cmd.execute(logic);
			} catch(Throwable err)
			{
				err.printStackTrace();
			}
		}
	}
	
	@Override
	public String toString()
	{
		NBTTagCompound compound = new NBTTagCompound();
		this.writeToNBT(compound);
		return compound + "";
	}
	
	public Set<PCBCommand> getCommands()
	{
		HashSet<PCBCommand> cs = new HashSet<PCBCommand>();
		for(PCBCommand cmd : this.cmds)
		{
			cmd.addChilds(cs);
		}
		return cs;
	}
	
	public void writeToNBT(NBTTagCompound compound)
	{
		NBTTagCompound nbt;
		NBTTagList commands = new NBTTagList();
		NBTTagList variables = new NBTTagList();
		for(PCBCommand cmd : this.cmds)
		{
			nbt = new NBTTagCompound();
			cmd.writeToNBT(nbt);
			nbt.setInteger("x", this.getCommandX(cmd));
			nbt.setInteger("y", this.getCommandY(cmd));
			commands.appendTag(nbt);
		}
		for(String key : this.vars.keySet())
		{
			nbt = new NBTTagCompound();
			nbt.setString("Key", key);
			nbt.setString("Val", this.vars.get(key));
			variables.appendTag(nbt);
		}
		compound.setTag("Commands", commands);
		compound.setTag("Vars", variables);
	}
	
	public void readFromNBT(NBTTagCompound compound)
	{
		NBTTagCompound nbt;
		int i;
		for(PCBCommand cmd : new HashSet<PCBCommand>(this.cmds))
		{
			this.remove(cmd.x, cmd.y);
		}
		NBTTagList commands = compound.getTagList("Commands", 10);
		NBTTagList variables = compound.getTagList("Vars", 10);
		for(i = 0; i < commands.tagCount(); ++i)
		{
			nbt = commands.getCompoundTagAt(i);
			PCBCommand cmd = new PCBCommand(this, "");
			cmd.readFromNBT(nbt);
			this.addCommand(nbt.getInteger("x"), nbt.getInteger("y"), cmd);
		}
		for(i = 0; i < variables.tagCount(); ++i)
		{
			nbt = variables.getCompoundTagAt(i);
			this.vars.put(nbt.getString("Key"), nbt.getString("Val"));
		}
	}
}

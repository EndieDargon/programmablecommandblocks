/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes: com.google.common.base.Throwables
 * net.minecraft.nbt.JsonToNBT net.minecraft.nbt.NBTException
 * net.minecraft.nbt.NBTTagCompound */
package com.zeitheron.progcommblocks.logic;

import com.google.common.base.Throwables;

import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;

public class PCBToJSON
{
	public static String toNBT(PCBCommandList list)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		list.writeToNBT(nbt);
		return nbt.toString();
	}
	
	public static PCBCommandList toPCB(String nbt)
	{
		PCBCommandList list = new PCBCommandList();
		try
		{
			list.readFromNBT(JsonToNBT.getTagFromJson(nbt));
		} catch(NBTException e)
		{
			Throwables.propagate(e);
		}
		return list;
	}
}

/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes: javax.annotation.Nullable
 * net.minecraft.command.CommandResultStats
 * net.minecraft.command.CommandResultStats$Type
 * net.minecraft.command.ICommandManager net.minecraft.command.ICommandSender
 * net.minecraft.entity.Entity net.minecraft.nbt.NBTBase
 * net.minecraft.nbt.NBTTagCompound net.minecraft.nbt.NBTTagList
 * net.minecraft.server.MinecraftServer
 * net.minecraft.tileentity.CommandBlockBaseLogic
 * net.minecraft.util.math.BlockPos net.minecraft.util.math.Vec3d
 * net.minecraft.util.text.ITextComponent
 * net.minecraft.util.text.TextComponentString net.minecraft.world.GameRules
 * net.minecraft.world.World */
package com.zeitheron.progcommblocks.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.command.CommandResultStats;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.CommandBlockBaseLogic;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

public class PCBCommand
{
	public final List<PCBCommand> childs = new ArrayList<PCBCommand>();
	public int x;
	public int y;
	public int min_successCount = 1;
	public int max_successCount = -1;
	public final PCBCommand parent;
	public final PCBCommandList list;
	public String command;
	
	public PCBCommand(PCBCommandList list, String cmd)
	{
		this.parent = null;
		this.list = list;
		this.command = cmd;
	}
	
	public PCBCommand(PCBCommand parent, String cmd)
	{
		this.parent = parent;
		this.list = parent.list;
		parent.childs.add(this);
		this.command = cmd;
	}
	
	public void addChilds(Collection<PCBCommand> cmd)
	{
		cmd.add(this);
		for(PCBCommand child : this.childs)
		{
			child.addChilds(cmd);
		}
	}
	
	public PCBCommandOutput execute(CommandBlockBaseLogic logic)
	{
		PCBCommandOutput out = new PCBCommandOutput(logic, this);
		this.executeChilds(logic, out);
		if(this.command.startsWith("!SETVAR ") && this.command.substring(8).contains(" "))
		{
			String varname = this.command.substring(8);
			varname = varname.substring(0, varname.indexOf(" "));
			this.list.vars.put(varname, out.toString());
		}
		return out;
	}
	
	public int size()
	{
		int size = 1;
		for(PCBCommand cmd : this.childs)
		{
			size += cmd.size();
		}
		return size;
	}
	
	public String getFormattedCommand()
	{
		String command = this.command;
		if(command.startsWith("!SETVAR ") && command.substring(8).contains(" "))
		{
			command = command.substring(9 + command.substring(8).indexOf(" "));
		}
		for(String var : this.list.vars.keySet())
		{
			if(!command.contains("VAR_" + var))
				continue;
			command = command.replaceAll("VAR_" + var + "#length", "" + this.list.vars.get(var).length() + "");
			command = command.replaceAll("VAR_" + var + "#trim", this.list.vars.get(var).trim());
			command = command.replaceAll("VAR_" + var, this.list.vars.get(var));
		}
		return command;
	}
	
	public void executeChilds(CommandBlockBaseLogic logic, PCBCommandOutput out)
	{
		for(PCBCommand child : this.childs)
		{
			if((out.successCount < child.min_successCount || out.successCount > child.max_successCount) && (child.max_successCount >= 0 || out.successCount < child.min_successCount))
				continue;
			child.execute(logic);
		}
	}
	
	public void writeToNBT(NBTTagCompound compound)
	{
		compound.setString("Command", this.command);
		NBTTagList childs = new NBTTagList();
		for(PCBCommand child : this.childs)
		{
			NBTTagCompound nbt = new NBTTagCompound();
			child.writeToNBT(nbt);
			childs.appendTag(nbt);
		}
		compound.setTag("Childs", childs);
		compound.setInteger("x", this.x);
		compound.setInteger("y", this.y);
	}
	
	public void readFromNBT(NBTTagCompound compound)
	{
		this.childs.clear();
		this.command = compound.getString("Command");
		NBTTagList childs = compound.getTagList("Childs", 10);
		for(int i = 0; i < childs.tagCount(); ++i)
		{
			NBTTagCompound nbt = childs.getCompoundTagAt(i);
			new PCBCommand(this, "").readFromNBT(nbt);
		}
		this.x = compound.getInteger("x");
		this.y = compound.getInteger("y");
	}
	
	public static class PCBCommandOutput implements ICommandSender
	{
		public final List<ITextComponent> output = new ArrayList<ITextComponent>();
		public final int successCount;
		public final ICommandSender logic;
		
		protected PCBCommandOutput(int successCount, ICommandSender logic)
		{
			this.successCount = successCount;
			this.logic = logic;
		}
		
		private PCBCommandOutput(CommandBlockBaseLogic logic, PCBCommand cmd)
		{
			this.logic = logic;
			this.output.clear();
			String cmd0 = cmd.getFormattedCommand();
			if(cmd0.startsWith("#"))
			{
				this.successCount = 1;
				this.sendMessage(new TextComponentString(cmd0.substring(1)));
			} else
			{
				this.successCount = logic.getServer().getCommandManager().executeCommand(this, cmd0);
			}
		}
		
		@Override
		public String getName()
		{
			return this.logic.getName();
		}
		
		@Override
		public ITextComponent getDisplayName()
		{
			return this.logic.getDisplayName();
		}
		
		@Override
		public void sendMessage(ITextComponent component)
		{
			this.output.add(component);
			if(this.logic.getServer() != null && this.logic.getEntityWorld().getGameRules().getBoolean("logAdminCommands"))
			{
				System.out.println(component.getFormattedText());
			}
		}
		
		@Override
		public boolean canUseCommand(int permLevel, String commandName)
		{
			return this.logic.canUseCommand(permLevel, commandName);
		}
		
		@Override
		public BlockPos getPosition()
		{
			return this.logic.getPosition();
		}
		
		@Override
		public Vec3d getPositionVector()
		{
			return this.logic.getPositionVector();
		}
		
		@Override
		public World getEntityWorld()
		{
			return this.logic.getEntityWorld();
		}
		
		@Override
		@Nullable
		public Entity getCommandSenderEntity()
		{
			return this.logic.getCommandSenderEntity();
		}
		
		@Override
		public boolean sendCommandFeedback()
		{
			return this.logic.sendCommandFeedback();
		}
		
		@Override
		public void setCommandStat(CommandResultStats.Type type, int amount)
		{
		}
		
		@Override
		@Nullable
		public MinecraftServer getServer()
		{
			return this.logic.getServer();
		}
		
		@Override
		public String toString()
		{
			String str = "";
			for(ITextComponent feedback : this.output)
			{
				str = str + feedback.getUnformattedText() + " ";
			}
			if(str.endsWith(" "))
			{
				str = str.substring(0, str.length() - 1);
			}
			return str;
		}
	}
	
}

/* Decompiled with CFR 0_123. */
package com.zeitheron.progcommblocks.logic;

import java.awt.Component;
import java.awt.HeadlessException;

import javax.swing.JDialog;
import javax.swing.JFileChooser;

public class JFileChooserAlwaysOnTop extends JFileChooser
{
	@Override
	protected JDialog createDialog(Component parent) throws HeadlessException
	{
		JDialog dg = super.createDialog(parent);
		dg.setAlwaysOnTop(true);
		return dg;
	}
}

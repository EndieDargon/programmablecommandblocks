package com.zeitheron.progcommblocks.logic;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import com.google.common.util.concurrent.AtomicDouble;
import com.zeitheron.hammercore.lib.zlib.io.IOUtils;
import com.zeitheron.progcommblocks.WebUtil;

public class PCBIO
{
	public static String upload(PCBCommandList list)
	{
		return PCBIO.upload(PCBToJSON.toNBT(list), "[ProgrammableCommandBlocks @VERSION@] PCB-Generated");
	}
	
	public static String upload(String text, String title)
	{
		try
		{
			return WebUtil.paste(text, "0ec2eb25b6166c0c27a394ae118ad829", title);
		} catch(RuntimeException e)
		{
			return e.getMessage();
		}
	}
	
	public static PCBCommandList download(String paste, AtomicDouble progress)
	{
		progress.set(0.0);
		try
		{
			URL link = new URL(paste);
			URLConnection connection = link.openConnection();
			InputStream in = connection.getInputStream();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			IOUtils.pipeData(in, out);
			in.close();
			byte[] buf = out.toByteArray();
			out = new ByteArrayOutputStream();
			String json = "";
			for(int i = 0; i < buf.length; ++i)
			{
				progress.set((double) i / (double) buf.length);
				Thread.sleep(2);
			}
			json = new String(buf);
			return PCBToJSON.toPCB(json);
		} catch(Throwable link)
		{
			return null;
		}
	}
	
	public static Throwable save(PCBCommandList list)
	{
		JFileChooserAlwaysOnTop chooser = new JFileChooserAlwaysOnTop();
		chooser.setDialogTitle("Save this PCB");
		chooser.setDialogType(1);
		int approved = chooser.showDialog(null, "Save");
		if(approved == 0)
		{
			String selstr = chooser.getSelectedFile().getAbsolutePath();
			if(!selstr.endsWith(".txt"))
			{
				selstr = selstr + ".txt";
			}
			File sel = new File(selstr);
			try
			{
				FileOutputStream fos = new FileOutputStream(sel);
				fos.write(PCBToJSON.toNBT(list).getBytes());
				fos.close();
			} catch(Throwable err)
			{
				return err;
			}
		}
		return null;
	}
}

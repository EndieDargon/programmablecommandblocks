/* Decompiled with CFR 0_123.
 * 
 * Could not load the following classes: org.lwjgl.input.Keyboard */
package com.zeitheron.progcommblocks.proxy;

import org.lwjgl.input.Keyboard;

public class ClientProxy extends CommonProxy
{
	@Override
	public boolean isSHIFTPressed()
	{
		return Keyboard.isKeyDown(42) || Keyboard.isKeyDown(54);
	}
}

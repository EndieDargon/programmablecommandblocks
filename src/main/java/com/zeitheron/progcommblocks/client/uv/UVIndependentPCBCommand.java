package com.zeitheron.progcommblocks.client.uv;

import org.lwjgl.opengl.GL11;

import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UV;
import com.zeitheron.progcommblocks.gui.GuiPCB;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.TextureMap;

public class UVIndependentPCBCommand extends UV
{
	public UVIndependentPCBCommand()
	{
		super(GuiPCB.gui, 84.0, 236.0, 20.0, 20.0);
	}
	
	@Override
	public void render(double x, double y)
	{
		super.render(x, y);
		Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		GL11.glPushMatrix();
		RenderUtil.drawTexturedModalRect(x + 2, y + 2, Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite("minecraft:blocks/command_block_front"), 16, 16);
		GL11.glPopMatrix();
		Minecraft.getMinecraft().getTextureManager().bindTexture(GuiPCB.gui);
	}
}
